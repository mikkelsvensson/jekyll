---
layout: post
title:  "Cellular development board"
date:   2023-02-09 10:00:00 +0200
categories: UCL
image: /assets/BC660K-GL DevBoard.jpg
---
I was doing an IoT project based on cellular connectivity and wanted to test the feasibility of the BC660K-GL from Quectel. Because of the low availability of the BC660K-GL Devkit, I designed my own. It only took two weeks to design the schematic and PCB layout in Altium Designer, order the components and PCB, wait for shipping and production, and then to hand-solder it. Most of that two weeks were shipping. That’s what I call rapid prototyping.
<figure align ="center">
<img src="/assets/BC660K-GL DevBoard.jpg" width="400" >
<figcaption><i>Quectel BC660K-GL</i></figcaption>
</figure>
The board has breakout pins for UART to communicate with an MCU. I used an STM32 Nucleo-32 and wrote a small program in C using the STM32CubeIDE to send AT commands to the module. The module worked as intended, but it was not right for the project.
